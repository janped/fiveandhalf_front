import Vue from 'vue';
import axios from 'axios';
import router from './router';
import auth from './auth';
import store from './store/index';

const setInterceptor = () =>
  // axios.interceptors.response.use(
  //   (response) => response,
  //   (error) => {
  //     // Do something with response error
  //     if (error.response.status === 401 && window.location.pathname !== '/login') {
  //       router.replace('/login');
  //     }
  //     return Promise.reject(error);
  //   });
  true
;

const setDefaultUrls = () => {
  axios.defaults.baseURL = process.env.NODE_ENV === 'production' ? 'production_endpoint' : process.env.API_ENDPOINT;
  return true;
};

const setHeaders = () => {
  axios.defaults.headers.common.Authorization = `Bearer ${localStorage.getItem('accessToken')}`;
  return true;
};

const setSnackbarMixin = () => {
  Vue.mixin({
    methods: {
      snack(type, message) {
        this.$store.dispatch('layout/setSnackbar', { type, message, });
      },
    },
  });
  return true;
};

const setAuthenticationGuard = () => {
  router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.guests)) {
      if (auth.checkAuth()) {
        next({
          path: '/pets',
        });
      } else next();
    } else if (to.matched.some(record => record.meta.auths)) {
      if (!auth.checkAuth()) {
        next({
          path: '/login',
        });
      } else next();
    } else next();
  });
  return true;
};

const checkLocalStorage = () => {
  if (localStorage.getItem('accessToken') !== null) {
    store.dispatch('auth/setToken', localStorage.getItem('accessToken'));
  }

  if (localStorage.getItem('expiration') !== null) {
    store.dispatch('auth/setExpiration', localStorage.getItem('expiration'));
  }

  if (localStorage.getItem('refreshToken') !== null) {
    store.dispatch('auth/setRefreshToken', localStorage.getItem('refreshToken'));
  }
  return true;
};

export default {
  client_id: 2,
  client_secret: (process.env.NODE_ENV === 'production') ?
    'XgnQdMDQMtIfkG7vSqu6tim6kdQmoyuMichwuO6L' : '47vpudVGb5ifa8B5kbnoboDHZzw6mDmDsXDRz6Zx',
  main_api: process.env.API_ENDPOINT,
  oauth_api: process.env.OAUTH_ENDPOINT,

  setConfigs: () => {
    /*eslint-disable */
    if (!setInterceptor()) {
      console.error('[CONFIG ERROR] Interceptors not set properly');
    }

    if (!setDefaultUrls()) {
      console.error('[CONFIG ERROR] API Enpoint could not be established');
    }

    if (!setHeaders()) {
      console.error('[CONFIG ERROR] Authorization headers not set at start');
    }

    if (!setAuthenticationGuard()) {
      console.error('[CONFIG ERROR] Authentication guard could not be set');
    }

    if (!setSnackbarMixin()) {
      console.error('[CONFIG ERROR] Global mixin for snackbars could not be set');
    }

    if (!checkLocalStorage()) {
      console.error('[CONFIG ERROR] Local storage could not been retrieved');
    }
    /* eslint-enable */
  },
};
