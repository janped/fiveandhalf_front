import Vue from 'vue';
import Router from 'vue-router';
import MasterTemplate from '../../src/layout/Master';
import LoginTemplate from '../../src/layout/Login';
import DashboardView from '../../src/components/pages/dashboard/DashboardView';
import PetsView from '../../src/components/pets/petsView';
import LoginView from '../../src/components/pages/auth/LoginView';
import PetView from '../../src/components/pets/petView';
import OrdersView from '../../src/components/orders/OrdersView';
import Error404 from '../../src/components/error/404';

Vue.use(Router);

/**
 * @TODO
 *
 * This routing mechanism is based on the fact, that it is not time-consuming
 * The legitimate way should include templates with default slots
 */
export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'MasterTemplate',
      component: MasterTemplate,
      meta: {
        auths: true,
      },
      children: [
        {
          path: '/',
          name: 'Dashboard',
          component: DashboardView,
          meta: {
            auths: true,
          },
        },
        {
          path: '/pets',
          name: 'Pets',
          component: PetsView,
          meta: {
            auths: true,
          },
        },
        {
          path: '/pet/:id',
          name: 'Pet',
          component: PetView,
          meta: {
            auths: true,
          },
        },
        {
          path: '/orders',
          name: 'Orders',
          component: OrdersView,
          meta: {
            auths: true,
          },
        },
      ],
    },
    {
      path: '/login',
      name: 'LoginTemplate',
      component: LoginTemplate,
      meta: {
        guests: true,
      },
      children: [
        {
          path: '/login',
          name: 'Login',
          component: LoginView,
          meta: {
            guests: true,
          },
        },
      ],
    },
    {
      path: '*',
      name: 'Unknown',
      component: Error404,
      meta: {
        guests: true,
      },
    },
  ],
});
