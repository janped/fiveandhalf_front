import qs from 'qs';
import axios from 'axios';
import config from './../config';

export default {
  getPets() {
    return axios.get(`${config.main_api}/pet`);
  },
  addPet(payload) {
    return axios.post(`${config.main_api}/pet`, payload);
  },
  updatePet(payload) {
    return axios.put(`${config.main_api}/pet`, payload);
  },

  // ------------------------------------------------------------------------------

  findByTags(params) {
    return axios.get(`${config.main_api}/pet/findByTags`, {
      params: {
        ...params,
      },
      paramsSerializer() {
        return qs.stringify(params, { indices: false, });
      },
    });
  },

  // ------------------------------------------------------------------------------

  getPet(id) {
    return axios.get(`${config.main_api}/pet/${id}`);
  },
  updatePetFormData(id, payload) {
    return axios.post(`${config.main_api}/pet/${id}`, payload);
  },
  removePet(id) {
    return axios.delete(`${config.main_api}/pet/${id}`);
  },

  // ------------------------------------------------------------------------------

  uploadImage(id) {
    return axios.post(`${config.main_api}/pet/${id}/uploadImage`);
  },
};
