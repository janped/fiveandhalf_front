import axios from 'axios';
import config from './../config';

export default {
  login(username, password) {
    // eslint-disable-next-line no-console
    console.log(config.oauth_api);
    return axios.post(`${config.oauth_api}/token`, {
      password,
      username,
      client_id: config.client_id,
      client_secret: config.client_secret,
      grant_type: 'password',
      scope: '',
    });
  },
  refreshToken(token) {
    return axios.post(`${config.oauth_api}/token`, {
      client_id: config.client_id,
      client_secret: config.client_secret,
      grant_type: 'refresh_token',
      scope: '',
      refresh_token: token,
    });
  },
};
