import axios from 'axios';
import config from './../config';

export default {
  placeOrder(payload) {
    return axios.post(`${config.main_api}/store/order`, payload);
  },
  // ------------------------------------------------------------------------------
  findPurchase(id) {
    return axios.get(`${config.main_api}/store/order/${id}`);
  },
  deletePurchase(id) {
    return axios.delete(`${config.main_api}/store/order/${id}`);
  },
  getOrders() {
    return axios.get(`${config.main_api}/store/order`);
  },
};
