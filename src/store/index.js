import Vue from 'vue';
import Vuex from 'vuex';
import auth from './modules/auth';
import layout from './modules/layout';
import pets from './modules/pets';
import orders from './modules/orders';

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    auth,
    layout,
    pets,
    orders,
  },
});
export default store;

Vue.use(
  Vuex,
  store,
);
