import wrapper from '../../auth';


const SET_LOGGING_TRUE = 'SET_LOGGING_TRUE';
const SET_LOGGING_FALSE = 'SET_LOGGING_FALSE';

const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
const LOGIN_FAILED = 'LOGIN_FAILED';

const SET_LOGIN_ERRORS = 'SET_LOGIN_ERRORS';

const UPDATE_TOKEN = 'UPDATE_TOKEN';
const UPDATE_EXPIRATION = 'UPDATE_EXPIRATION';
const UPDATE_REFRESH_TOKEN = 'UPDATE_REFRESH_TOKEN';
const LOGOUT = 'LOGOUT';

const authStore = {
  namespaced: true,
  state: {
    accessToken: null,
    expiration: null,
    refreshToken: null,
    loginErrors: [],
    loginStatus: false,
    logging: false,
  },
  getters: {
    errors: state => state.loginErrors,
    logging: state => state.logging,
    status: state => state.loginStatus,
    token: state => state.accessToken,
    expiration: state => state.expiration,
    refreshToken: state => state.refreshToken,
  },
  actions: {
    logout({ commit, }) {
      wrapper.logout();
      commit(LOGOUT);
    },
    login({ commit, }, { email, password, }) {
      commit(SET_LOGGING_TRUE);
      return wrapper.login(email, password).then(
        () => {
          commit(LOGIN_SUCCESS);
        },
        (error) => {
          commit(LOGIN_FAILED, error.response.data.message);
        },
      );
    },
    clearErrors({ commit, }) {
      commit(SET_LOGIN_ERRORS, []);
    },
    setToken: ({ commit, }, value) => {
      commit(UPDATE_TOKEN, value);
    },
    setExpiration: ({ commit, }, value) => {
      commit(UPDATE_EXPIRATION, value);
    },
    setRefreshToken: ({ commit, }, value) => {
      commit(UPDATE_REFRESH_TOKEN, value);
    },
  },
  mutations: {
    [SET_LOGGING_TRUE]: (state) => {
      state.logging = true;
    },
    [SET_LOGGING_FALSE]: (state) => {
      state.logging = false;
    },
    [LOGOUT]: (state) => {
      state.loginStatus = false;
    },
    [LOGIN_SUCCESS]: (state) => {
      state.loginStatus = true;
      state.loginErrors = [];
      state.logging = false;
    },
    [LOGIN_FAILED]: (state, errors) => {
      state.loginStatus = false;
      state.loginErrors = errors;
      state.logging = false;
    },
    [SET_LOGIN_ERRORS]: (state, errors) => {
      state.loginErrors = errors;
    },
    [UPDATE_TOKEN]: (state, value) => {
      state.accessToken = value;
    },
    [UPDATE_EXPIRATION]: (state, value) => {
      state.expiration = value;
    },
    [UPDATE_REFRESH_TOKEN]: (state, value) => {
      state.refreshToken = value;
    },
  },
};
export default authStore;
