const layoutStore = {
  namespaced: true,
  state: {
    clipped: false,
    drawer: true,
    snackbar: {
      snackbar: false,
      color: 'success', // success, info, error
      timeout: 6000,
      text: '',
    },
    items: [
      {
        icon: 'dashboard',
        title: 'Dashboard',
        url: '/',
      },
      {
        icon: 'pets',
        title: 'Pets',
        url: '/pets',
        divider: true,
      },
      {
        icon: 'shopping_cart',
        title: 'Orders',
        url: '/orders',
      },
    ],
    right: true,
  },
  getters: {},
  actions: {
    async setSnackbar({ commit, }, { type, message, }) {
      await commit('UPDATE_SNACKBAR_STATE', type);
      await commit('UPDATE_SNACKBAR_VALUE', message);
      await commit('CALL_SNACKBAR');
    },
    closeSnackbar({ commit, }) {
      commit('CALL_SNACKBAR', false);
    },
  },
  mutations: {
    UPDATE_DRAWER: (state, value) => {
      if (value !== undefined) {
        state.drawer = value;
      }
    },
    UPDATE_SNACKBAR_STATE: (state, value) => {
      if (value !== undefined) {
        state.snackbar.color = value;
      }
    },
    UPDATE_SNACKBAR_VALUE: (state, value) => {
      if (value !== undefined) {
        state.snackbar.text = value;
      }
    },
    CALL_SNACKBAR: (state, show = true) => {
      state.snackbar.snackbar = show;
    },
  },
};
export default layoutStore;
