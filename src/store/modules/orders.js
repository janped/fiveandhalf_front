import api from '../../api/storeAPI';

const SET_ORDERS = 'SET_ORDERS';

const ordersStore = {
  namespaced: true,
  state: {
    orders: [],
  },
  getters: {
    noOrders: state => (state.orders.length === 0),
    allOrders: state => state.orders,
  },
  actions: {
    async getOrders({ commit, getters, }, force = false) {
      // Check employees state
      if (getters.noOrders || force) {
        // If none, call API to get new ...
        await api.getOrders().then(
          (orders) => {
            commit(SET_ORDERS, orders.data);
          },
          // eslint-disable-next-line
          (err) => console.error(`[STORE ERROR] Could not get orders: ${err.response}`)
        );
      }
      return new Promise(resolve => resolve(getters.allOrders));
    },
  },
  mutations: {
    [SET_ORDERS]: (state, value) => {
      if (value !== undefined) {
        state.orders = value;
      }
    },
  },
};
export default ordersStore;
