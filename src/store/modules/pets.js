import * as _ from 'lodash';
import api from '../../api/petAPI';

const SET_PETS = 'SET_PETS';
const SET_PET = 'SET_PET';
const CREATE_PET = 'CREATE_PET';
const REMOVE_PET = 'REMOVE_PET';

const petStore = {
  namespaced: true,
  state: {
    pets: [],
    pet: null,
    categories: {
      dog: {
        text: 'Dog',
        id: 1,
      },
      cat: {
        text: 'Cat',
        id: 2,
      },
      horse: {
        text: 'Horse',
        id: 3,
      },
      bird: {
        text: 'Bird',
        id: 4,
      },
    },
    tags: {
      big: {
        text: 'big',
        id: 1,
      },
      small: {
        text: 'small',
        id: 2,
      },
      barks: {
        text: 'barks',
        id: 3,
      },
      flies: {
        text: 'flies',
        id: 4,
      },
      colorful: {
        text: 'colorful',
        id: 5,
      },
    },
  },
  getters: {
    noPets: state => (state.pets.length === 0),
    petId: state => state.pet.id,
    storePet: state => state.pet,
    allPets: state => state.pets,
    allPetsNumber: state => state.pets.length,
    allCategories: state => state.categories,
    allCategoriesNames: state => _.map(state.categories, c => c.text),
    allTags: state => state.tags,
    allTagsNames: state => _.map(state.tags, t => t.text),
  },
  actions: {
    async getPets({ commit, getters, }, force = false) {
      // Check employees state
      if (getters.noPets || force) {
        // If none, call API to get new ...
        await api.getPets().then(
          (pets) => {
            commit(SET_PETS, pets.data);
          },
          // eslint-disable-next-line
          (err) => console.error(`[STORE ERROR] Could not get pets: ${err.response}`)
        );
      }
      return new Promise(resolve => resolve(getters.allPets));
    },
    async getPetsFiltered({ commit, }, query) {
      await api.findByTags(query).then(
        (pets) => {
          commit(SET_PETS, pets.data);
        },
        // eslint-disable-next-line
        (err) => console.error(`[STORE ERROR] Could not get filtered pets: ${err.response}`)
      );
    },
    async create({ commit, }, pet) {
      await api.addPet(pet).then(
        (res) => {
          commit(CREATE_PET, res.data);
        },
        // eslint-disable-next-line
        (err) => console.error(`[STORE ERROR] Could not create pet: ${err.response}`)
      );
    },
    async update({ commit, }, pet) {
      await api.updatePet(pet).then(
        (res) => {
          commit(SET_PET, res.data);
        },
        // eslint-disable-next-line
        (err) => console.error(`[STORE ERROR] Could not update pet: ${err.response}`)
      );
    },
    async remove({ commit, }, petId) {
      await api.removePet(petId).then(
        res => commit(REMOVE_PET, res.id),
        // eslint-disable-next-line
        (err) => console.error(`[STORE ERROR] Could not remove pet: ${err.response}`)
      );
    },
    setPet({ commit, }, pet) {
      commit(SET_PET, pet);
    },
    async getPet({ commit, }, petId) {
      await api.getPet(petId).then(
        res => commit(SET_PET, res.data),
        // eslint-disable-next-line
        (err) => console.error(`[STORE ERROR] Could not get pet: ${err.response}`)
      );
    },
  },
  mutations: {
    [SET_PETS]: (state, value) => {
      if (value !== undefined) {
        state.pets = value;
      }
    },
    [SET_PET]: (state, value) => {
      if (value !== undefined) {
        state.pet = value;
      }
    },
    [CREATE_PET]: (state, value) => {
      if (value !== undefined) {
        state.pets.push(value);
        state.pet = value;
      }
    },
    [REMOVE_PET]: (state, petId) => {
      if (petId !== undefined) {
        state.pets = _.filter(state.pets, p => p.id !== petId);
      }
    },
  },
};
export default petStore;
