import axios from 'axios';
import api from './api/authAPI';
import store from './store/index';

/**
 * Checks if the current access token is valid and refreshes it if one is stored but expired
 * @returns {boolean}
 */
const checkAuth = () => {
  // Check if token exists
  if (localStorage.getItem('accessToken') !== null) {
    // Check if token is not expired
    if (!tokenExpired()) {
      // Update store instance with current set token
      store.dispatch('auth/setToken', localStorage.getItem('accessToken'));
      return true;
    }
    // @TODO Implement refresh token functionality
    // refreshToken();
  }
  return false;
};

/**
 * Gets new access token for logging user
 * @param email
 * @param password
 */
const login = (email, password) => api.login(email, password).then(
  (response) => {
    setToken(response.data.access_token);
    setExpiration(response.data.expires_in);
    setRefreshToken(response.data.refresh_token);
    axios.defaults.headers.common.Authorization = `Bearer ${response.data.access_token}`;
  },
);

/**
 * Set the access token to null (in localStorage and store/auth) to logout
 */
const logout = () => {
  setToken(null);
  setExpiration(null);
  setRefreshToken(null);
};

/**
 * Handles the update of the access token (in localStorage and store/auth)
 * Additional param routeTo allows to set a router push to given route
 * @param token
 */
const setToken = (token) => {
  store.dispatch('auth/setToken', token).then(
    () => {
      token ? localStorage.setItem('accessToken', token) : localStorage.removeItem('accessToken');
    },
    (err) => {
      console.log(`Could not update auth store with error: ${err}`); // eslint-disable-line no-console
    },
  );
};

const setExpiration = (timestamp) => {
  const expirationTimestamp = (timestamp) ? Number(new Date().getTime() + timestamp) : false;
  store.dispatch('auth/setExpiration', expirationTimestamp).then(
    () => {
      expirationTimestamp ? localStorage.setItem('expiration', expirationTimestamp)
        : localStorage.removeItem('expiration');
    },
    (err) => {
      console.log(`Could not update auth store with error: ${err}`); // eslint-disable-line no-console
    },
  );
};

const setRefreshToken = (token) => {
  store.dispatch('auth/setRefreshToken', token).then(
    () => {
      token ? localStorage.setItem('refreshToken', token) : localStorage.removeItem('refreshToken');
    },
    (err) => {
      console.log(`Could not update auth store with error: ${err}`); // eslint-disable-line no-console
    },
  );
};

/**
 * Checks if the access token is expired:  expired = token.expiration_date - 3 minutes
 * @returns {boolean}
 */
const tokenExpired = () => {
  const expiration = localStorage.getItem('expiration');

  const now = Number(new Date().getTime());
  const expiresAt = Number(new Date().getTime() + Number(expiration * 1000));

  if (now > expiresAt) {
    // @TODO Implement refresh token functionality
    // refreshToken();
    return true;
  }
  return false;
};

export default {
  checkAuth,
  login,
  logout,
  setToken,
};
