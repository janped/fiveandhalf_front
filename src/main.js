// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import 'vuetify/dist/vuetify.min.css';
import Vue from 'vue';
import Vuetify from 'vuetify';
import { sync, } from 'vuex-router-sync';
import App from './App';
import router from './router';
import store from './store';
import config from './config';
import theme from './assets/theme';

sync(store, router);
Vue.use(Vuetify, theme);

Vue.config.productionTip = false;

// eslint-disable-next-line no-console
console.log('Set default configs');
config.setConfigs();

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App, },
  template: '<App/>',
});
