module.exports = {
  NODE_ENV: '"production"',
  API_ENDPOINT: '"http://five-api.mimosa-soft.com/api"',
  OAUTH_ENDPOINT: '"http://five-api.mimosa-soft.com/oauth"',
};
