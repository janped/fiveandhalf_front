# 5.5 - Take Home Excersise
---

### 1 Additional notes

* Based on Vue 2 & the vuetify package + Vuex (Redux)
* Did make just the sample actions (prototype)
* FindByTags function was based more on the front-end (performance wise better), but the functionality still
is available on the back-end

### 2 How was it set-up

#### 2.1 Deployment
* Based on docker container - did not bother to make a custom dockerfile, did base it on 
the ubuntu:latest image (prototype)
* Container architecture is based on Nginx reverse proxy by jwilder
* Inside the container, apache2 was used with the app

### 3 Setting up locally
* `npm install`
* `npm run dev`

### Contact
**For more information:**  
Email: jan.pedryc@gmail.com  
Phone: +48 727 698 376
